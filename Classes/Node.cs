﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace BinaryTree;

public class BTreeNode<TItem>
    where TItem : IComparable
{
    public bool IsLeaf { get; private set; }
    public List<TItem> Items { get; private set; }
    public List<BTreeNode<TItem>> Descendants { get; private set; }
    public BTreeNode<TItem> Parent { get; set; }
    
    public BTreeNode(bool isLeaf, BTreeNode<TItem> parent=null)
    {
        IsLeaf = isLeaf;
        Parent = parent;
        Descendants = new List<BTreeNode<TItem>>();
        Items = new List<TItem>();
    }
    
    public int GetDescendantIndex(TItem key) 
    {
        for (var i = 0; i < Items.Count; i++)
            if (key.CompareTo(Items[i]) == -1) 
                return i;
        
        return Items.Count;
    }
    
}