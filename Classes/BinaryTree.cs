﻿using System;

namespace BinaryTree;

public class BinaryTree<TItem>
    where TItem : IComparable
{
    private int _rank;
    private BTreeNode<TItem> _root;

    public BinaryTree(int rank)
    {
        _rank = rank;
        _root = null;
    }
    
    //Не трогать
    public void Add(TItem item)
    {
        if (_root == null)
            return;
        Add(item, _root);
    }
    
    /// <summary>
    /// Добавление элемента в дерево (Нияз)
    /// O(Nlog(N))
    /// Я советую бежать по списку значений и воспользовать методом Insert в нужный момент, чтобы не сортировать
    /// </summary>
    /// <param name="item"></param>
    /// <param name="root"></param>
    /// <exception cref="NotImplementedException"></exception>
    public void Add(TItem item, BTreeNode<TItem> root)
    {
        throw new NotImplementedException();
    }
    
    //Не трогать
    public void Delete(TItem item)
    {
        if (_root == null)
            return;
        
        Delete(item, _root);
    }
    
    /// <summary>
    /// Удаление элемента из дерева (Илья)
    /// O(Nlog(N))
    /// </summary>
    /// <param name="item"></param>
    /// <param name="root"></param>
    private void Delete(TItem item, BTreeNode<TItem> root)
    {
        
    }
    
    
    public TItem Find(TItem key)
    {
        throw new NotImplementedException();
    }
    
}